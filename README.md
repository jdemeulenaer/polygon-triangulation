---------------------
Polygon triangulation
---------------------
This library implements the polygon triangulation algorithm described in "Computational Geometry : Algorithms and Applications" (3rd edition) by de Berg et al. This algorithm triangulates any simple polygon by decomposing it into y-monotone pieces and then triangulates each piece into triangles.
A GUI is available to test the library and apply the triangulation for the Art Gallery Problem.
