/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package geometry;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent a triangle.
 */
public class Triangle {

    private Vertex[] vertices;
    private List<Triangle> neighbours;
    public boolean visited;

    /**
     * Create a triangle composed of 3 vertices.
     */
    public Triangle(Vertex v1, Vertex v2, Vertex v3) {
        this.vertices = new Vertex[] { v1, v2, v3 };
        this.neighbours = new ArrayList<Triangle>(3);
        this.visited = false;
    }

    /**
     * Create a triangle composed of 3 vertices.
     * @param vertices
     */
    public Triangle(List<Vertex> vertices) {
        this(vertices.get(0), vertices.get(1), vertices.get(2));
    }

    /**
     * Return the vertices of the triangle.
     */
    public Vertex[] getVertices() {
        return vertices;
    }

    /**
     * Return the triangle neighbours of this triangle.
     */
    public List<Triangle> getNeighbours() {
        return neighbours;
    }

    /**
     * Add a triangle as a neighbour of this triangle.
     * @param n the neighbour of this triangle.
     */
    public void addNeighbour(Triangle n) {
        this.neighbours.add(n);
    }

    /**
     * Return the hash of each edge composing this triangle.
     */
    public String[] getEdgesHashStrings() {
        return new String[] {
                edgeHashString(vertices[0], vertices[1]),
                edgeHashString(vertices[1], vertices[2]),
                edgeHashString(vertices[2], vertices[0])
        };
    }

    private String edgeHashString(Vertex origin, Vertex destination) {
        if (origin.below(destination)) {
            return origin.hashString() + "->" + destination.hashString();
        }
        else {
            return destination.hashString() + "->" + origin.hashString();
        }
    }

}
