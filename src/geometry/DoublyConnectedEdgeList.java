/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package geometry;

import java.util.List;
import java.util.ArrayList;

/**
 * This class implements the Doubly Connected Edge List (DCEL) data structure as described in "Computational Geometry :
 * Algorithms and Applications" (3rd edition).
 */
public class DoublyConnectedEdgeList {

    private class Diagonal {
        public Vertex v1;
        public Vertex v2;
        public Diagonal(Vertex v1, Vertex v2) {
            this.v1 = v1;
            this.v2 = v2;
        }
    }

	private List<Vertex> vertices = new ArrayList<Vertex>();
    private List<HalfEdge> edges = new ArrayList<HalfEdge>();
    private List<Face> faces = new ArrayList<Face>();
    private List<Diagonal> diagonals = new ArrayList<Diagonal>();

    /**
     * Create a Doubly Connected Edge List.
     * At first, the DCEL is composed of only one face and we initialize each vertices and their corresponding half-
     * edges.
     * @param coords the coordinates of the vertices of the DCEL, given in counter clockwise order
     */
    public DoublyConnectedEdgeList(Vector2D[] coords) {
        Face face = new Face();
        faces.add(face);

        HalfEdge prevLeftEdge = null;
        HalfEdge prevRightEdge = null;

        for (Vector2D coord : coords) {
            Vertex vertex = new Vertex();
            HalfEdge left = new HalfEdge();
            HalfEdge right = new HalfEdge();

            left.face = face;
            left.next = null;
            left.origin = vertex;
            left.twin = right;

            right.face = null;
            right.next = prevRightEdge;
            right.origin = null;
            right.twin = left;

            edges.add(left);
            edges.add(right);

            vertex.out = left;
            vertex.coord = coord;

            vertices.add(vertex);

            if (prevLeftEdge != null) {
                prevLeftEdge.next = left;
            }

            if (prevRightEdge != null) {
                prevRightEdge.origin = vertex;
            }

            prevLeftEdge = left;
            prevRightEdge = right;
        }

        HalfEdge firstLeftEdge = edges.get(0);
        prevLeftEdge.next = firstLeftEdge;

        HalfEdge firstRightEdge = edges.get(1);
        firstRightEdge.next = prevRightEdge;

        prevRightEdge.origin = vertices.get(0);

        face.edge = firstLeftEdge;
    }

    /**
     * Return the vertices of the DCEL.
     */
    public List<Vertex> getVertices() {
        return this.vertices;
    }

    /**
     * Return the edges of the DCEL.
     */
    public List<HalfEdge> getEdges() {
        return this.edges;
    }

    /**
     * Return the faces of the DCEL.
     * @return
     */
    public List<Face> getFaces() {
        return this.faces;
    }

    /**
     * Add a diagonal between two vertices.
     * @param v1 the first edge adjacent to the diagonal
     * @param v2 the other edge adjacent to the diagonal
     */
    public void addDiagonal(Vertex v1, Vertex v2) {
        diagonals.add(new Diagonal(v1, v2));
    }

    private void insertDiagonals() {
        for (Diagonal diagonal : diagonals) {
            Vertex v1 = diagonal.v1;
            Vertex v2 = diagonal.v2;

            Face face = new Face();

            HalfEdge left = new HalfEdge();
            HalfEdge right = new HalfEdge();

            Face referenceFace = this.getReferenceFace(v1, v2);
            HalfEdge prev1 = this.getPreviousEdge(v1, referenceFace);
            HalfEdge prev2 = this.getPreviousEdge(v2, referenceFace);

            face.edge = left;
            referenceFace.edge = right;

            left.face = face;
            left.next = prev2.next;
            left.origin = v1;
            left.twin = right;

            right.face = referenceFace;
            right.next = prev1.next;
            right.origin = v2;
            right.twin = left;

            prev1.next = left;
            prev2.next = right;

            HalfEdge curr = left.next;
            while (curr != left) {
                curr.face = face;
                curr = curr.next;
            }

            this.edges.add(left);
            this.edges.add(right);

            this.faces.add(face);
        }
    }

    /**
     * Decompose this DCEL into the smaller ones that are formed by the diagonals.
     * @return the list of smaller DCEL
     */
    public List<DoublyConnectedEdgeList> decompose() {
        this.insertDiagonals();
        List<DoublyConnectedEdgeList> polygons = new ArrayList<DoublyConnectedEdgeList>(this.faces.size());
        for (Face face : this.faces) {
            int size = face.getEdgeCount();
            HalfEdge left = face.edge;
            Vector2D[] vertices = new Vector2D[size];
            vertices[0] = left.origin.coord;
            left = left.next;
            int j = 1;
            while (left != face.edge) {
                vertices[j++] = left.origin.coord;
                left = left.next;
            }

            DoublyConnectedEdgeList subPolygon = new DoublyConnectedEdgeList(vertices);
            polygons.add(subPolygon);
        }
        return polygons;
    }

    private Face getReferenceFace(Vertex v1, Vertex v2) {
        if (v1.out.face == v2.out.face) {
            return v1.out.face;
        }

        HalfEdge e1 = v1.out.twin.next.twin;
        while (e1 != v1.out.twin) {
            HalfEdge e2 = v2.out.twin.next.twin;
            while (e2 != v2.out.twin) {
                if (e1.face == e2.face) {
                    return e1.face;
                }
                e2 = e2.next.twin;
            }
            e1 = e1.next.twin;
        }

        return v1.out.face;
    }

    private HalfEdge getPreviousEdge(Vertex vertex, Face face) {
        HalfEdge twin = vertex.out.twin;
        HalfEdge edge = vertex.out.twin.next.twin;

        while (edge != twin) {
            if (edge.face == face) {
                return edge;
            }
            edge = edge.next.twin;
        }
        return edge;
    }

}
