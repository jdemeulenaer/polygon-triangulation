/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package geometry;

import java.util.Locale;

/**
 * Represent a vertex of a Doubly Connected Edge List.
 */
public class Vertex implements Comparable<Vertex> {

	public enum Chain { LEFT, RIGHT }
	public enum VertexType { REGULAR_LEFT, REGULAR_RIGHT, START, END, MERGE, SPLIT }

	public Vector2D coord;
	public HalfEdge out;
	private VertexType type;
	public Chain chain;

	/**
	 * Return the half edge entering this vertex.
	 */
	public HalfEdge getInEdge() {
		return out.getPrevious();
	}

	/**
	 * Return the half edge going out from this vertex.
	 */
	public HalfEdge getOutEdge() {
		return this.out;
	}

	/**
	 * Compare this vertex to another. A vertex is smaller to another if it is higher (this.y < other.y) or if it is
	 * at the same height and on the left of it (this.y == other.y && this.x < other.x).
	 * @param other the other vertex to compare with
	 */
	@Override
	public int compareTo(Vertex other) {
		if (this.coord.y > other.coord.y) {
			return -1;
		}
		else if (this.coord.y == other.coord.y && this.coord.x < other.coord.x) {
			return -1;
		}
		else if (this.coord.y == other.coord.y && this.coord.x == other.coord.x) {
			return 0;
		}
		else {
			return 1;
		}
	}

	/**
	 * Return the type of this vertex : start, end, split, merge or regular.
	 */
	public VertexType getType() {
		if (this.type == null) {
			computeType();
		}

		return this.type;
	}

	private void computeType() {
		Vertex prev = this.out.getPrevious().getOrigin();
		Vertex next = this.out.getDestination();

		if (prev.below(this) && next.below(this)) {
            /* Split or start vertex */
			if (isConvex(prev, next, this)) {
				this.type = VertexType.START;
			}
			else {
				this.type = VertexType.SPLIT;
			}
		}
		else if (this.below(prev) && this.below(next)) {
            /* Merge or end vertex */
			if (isConvex(prev, next, this)) {
				this.type = VertexType.END;
			}
			else {
				this.type = VertexType.MERGE;
			}
		}
		else {
            /* Regular vertex */
			if (this.below(prev)) {
                /* Polygon lies on the right */
				this.type = VertexType.REGULAR_RIGHT;
			}
			else {
                /* Polygon lies on the left */
				this.type = VertexType.REGULAR_LEFT;
			}

		}
	}

	public static boolean isConvex(Vertex p1, Vertex p2, Vertex p3) {
		double tmp =( (p3.coord.y - p1.coord.y) * (p2.coord.x - p1.coord.x) - (p3.coord.x - p1.coord.x) * (p2.coord.y - p1.coord.y));
		return tmp < 0.0f;
	}

	public boolean below(Vertex other) {
		if (this.coord.y < other.coord.y) {
			return true;
		}
		else if (this.coord.y == other.coord.y) {
			return this.coord.x > other.coord.x;
		}
		return false;
	}

	public String hashString() {
		return this.coord.x + ";" + this.coord.y;
	}

	@Override
	public String toString() {
		return String.format(new Locale("en_US"), "(%.1f, %.1f)", this.coord.x, this.coord.y);
	}

}
