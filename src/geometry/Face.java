/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package geometry;

/**
 * Represent a face in a Doubly Connected Edge List.
 */
public class Face {

    public HalfEdge edge;

    /**
     * Return the number of edges composing this face.
     */
    public int getEdgeCount() {
        HalfEdge edge = this.edge;
        int count = 0;
        if (edge != null) {
            count++;
            while (edge.next != this.edge) {
                count ++;
                edge = edge.next;
            }
        }
        return count;
    }

}
