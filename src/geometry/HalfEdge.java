/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package geometry;

/**
 * Represent a half-edge between two vertices in a Doubly Connected Edge List.
 */
public class HalfEdge implements Comparable<HalfEdge> {

    public Vertex origin;
    public HalfEdge twin;
    public HalfEdge next;
    public Face face;

    /**
     * Return the origin vertex of this edge.
     */
    public Vertex getOrigin() {
        return this.origin;
    }

    /**
     * Return the destination vertex of this edge.
     */
    public Vertex getDestination() {
        return this.next.origin;
    }

    /**
     * Return the edge previous to this edge.
     */
    public HalfEdge getPrevious() {
        HalfEdge edge = twin.next.twin;
        while (edge.next != this) {
            edge = edge.next.twin;
        }
        return edge;
    }

    /**
     * Compare this edge to another.
     * @param other the other edge
     */
    public int compareTo(HalfEdge other) {
        if (this == other) {
            return 0;
        }

        if (other.getOrigin().coord.y == other.getDestination().coord.y) {
            if (this.getOrigin().coord.y == this.getDestination().coord.y) {
                if(this.getOrigin().coord.y < other.getOrigin().coord.y) {
                    return 1;
                } else {
                    return -1;
                }
            }
            if (Vertex.isConvex(this.getOrigin(), this.getDestination(), other.getOrigin())) {
                return 1;
            } else {
                return -1;
            }
        } else if (this.getOrigin().coord.y == this.getDestination().coord.y) {
            if (Vertex.isConvex(other.getOrigin(), other.getDestination(), this.getOrigin())) {
                return -1;
            } else {
                return 1;
            }
        } else if(this.getOrigin().coord.y < other.getOrigin().coord.y) {
            if(Vertex.isConvex(other.getOrigin(), other.getDestination(), this.getOrigin())) {
                return -1;
            }
            else {
                return 1;
            }
        } else {
            if(Vertex.isConvex(this.getOrigin(), this.getDestination(), other.getOrigin())) {
                return 1;
            }
            else {
                return -1;
            }
        }
    }

}
