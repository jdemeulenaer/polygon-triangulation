/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package benchmark;

import geometry.DoublyConnectedEdgeList;
import geometry.Vector2D;
import naive.NaiveTriangularisation;
import naive.SimplePolygon;
import triangularisation.MonotoneDecomposition;
import triangularisation.TriangulateMono;

import java.util.List;

public class Benchmarking {

    private static long solveMonotone(Vector2D[] vertices) {
        DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
        long totalTime = 0;

        /* Decomposition into monotone pieces */
        long startTime = System.currentTimeMillis();
        MonotoneDecomposition.makeMonotone(polygon);
        long endTime = System.currentTimeMillis();
        totalTime += endTime - startTime;

        List<DoublyConnectedEdgeList> subPolygons = polygon.decompose();

        /* Decomposition of monotone triangles*/
        startTime = System.currentTimeMillis();
        for (DoublyConnectedEdgeList subPolygon : subPolygons) {
            TriangulateMono.triangulateMonotonePolygon(subPolygon);
        }
        endTime = System.currentTimeMillis();
        totalTime += endTime - startTime;

        return totalTime;
    }

    private static long solveNaive(Vector2D[] vertices) {
        long startTime = System.currentTimeMillis();
        SimplePolygon polygon = new SimplePolygon(vertices);
        NaiveTriangularisation.triangulateEarClipping(polygon);
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static void main(String[] args){
        int[] n = { 10, 100, 500, 1000, 2000, 5000, 7500, 10000, 20000};
        int iter = 5;
        int size = n.length;

        double[] naiveTimes = new double[size];
        double[] normalTimes = new double[size];
        for (int j = 0; j < size; j++) {

            long naiveTime = 0;
            long monotoneTime = 0;
            for(int i = 0; i < iter ; i++) {
                Vector2D[] vertices = PolygonGenerator.generateVertices(10, 10, 1000, 1, 1, n[j]);
                naiveTime += solveNaive(vertices);
                monotoneTime += solveMonotone(vertices);
            }

            System.out.printf("For %d vertices: \n\tNaive time: %d \n\tMonotone time: %d \n",n[j], naiveTime, monotoneTime);
            naiveTimes[j] = (double) naiveTime / (double) monotoneTime;
            normalTimes[j] = 1.0;
        }

        double mult = 1;
        for (double i : naiveTimes) {
            System.out.println(i);
            mult *= i;
        }
        System.out.printf("\nGeometric mean: \nNaive: %f \nMonotone: %f", Math.pow(mult, 1 / (double) size), 1.0);
    }

}
