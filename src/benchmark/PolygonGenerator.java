/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package benchmark;

import java.util.Random;

import geometry.Vector2D;

public class PolygonGenerator {

	/**
     * Generates the coordinates of the points of a simple polygon in CCW order.
     * @param x the x coordinate of the center
     * @param y the y coordinate of the center
     * @param aveRadius the average distance between the center and the points
     * @param irregularity the random factor in the angle steps between each points (from 0 to 1)
     * @param spikeyness the random factor in the distance between the center and the points (from 0 to 1)
     * @param n the number of points you want
     * @return an array of Vector2D[] representing a simple polygon in CCW order.
     */
	public static Vector2D[] generateVertices(int x, int y, int aveRadius, double irregularity, double spikeyness, int n) {
		
		Vector2D[] vertices = new Vector2D[n];
		irregularity = clip( irregularity, 0,1 ) * 2*Math.PI / n;
	    spikeyness = clip( spikeyness, 0,1 ) * aveRadius;
	
	    double[] angleSteps = new double[n];
	    double lower = (2*Math.PI / n) - irregularity;
	    double upper = (2*Math.PI / n) + irregularity;
	    double sum = 0;
	    double tmp;
	    for(int i= 0 ; i < n; i++) {
	    	tmp = lower + Math.random()*upper; 
	    	angleSteps[i]= tmp;
	    	sum = sum + tmp;
	    }
	    
	    
	    double k = sum / (2*Math.PI);
	    for(int i= 0 ; i < n; i++) {
	    	angleSteps[i] = angleSteps[i]/k;
	    }
	    
	    Random rand = new Random();
	    double angle = Math.random()*2*Math.PI; 
	    double epsilon = aveRadius/3;
	    double r,xx,yy;
	    for(int i= 0 ; i < n; i++) {
	        r = clip(aveRadius+rand.nextGaussian()*spikeyness, 0, 2*aveRadius );
	        xx = x + (r + epsilon)*Math.cos(angle);
	        yy = y + (r + epsilon)*Math.sin(angle);
	        vertices[i] = new Vector2D(xx,yy);

	        angle = angle + angleSteps[i];
	    }
	        
		return vertices;
	}
	
	private static double clip(double x, double min, double max) {
	     if (min > max) {
	    	 return x;    
	     }
	     else if (x < min) {
	    	 return min;
	     }
	     else if (x > max) {
	    	 return max;
	     }
	     else {
	    	 return x;
	     }
	}

}
