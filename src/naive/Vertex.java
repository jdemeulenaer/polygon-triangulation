/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package naive;

/**
 * Represents a vertex in a polygon.
 */
public class Vertex {

    public double x;
    public double y;
    private Edge inEdge;
    private Edge outEdge;
    
    /**
     * Create a vertex.
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public Vertex(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Return the edge arriving at this vertex.
     */
    public Edge getInEdge() {
        return this.inEdge;
    }

    /**
     * Set the edge arriving at this vertex.
     */
    public void setInEdge(Edge edge) {
        this.inEdge = edge;
    }

    /**
     * Return the edge leaving this vertex.
     */
    public Edge getOutEdge() {
        return this.outEdge;
    }

    /**
     * Set the edge leaving this vertex.
     */
    public void setOutEdge(Edge edge) {
        this.outEdge = edge;
    }

    /**
     * Returns true if the angle between p1,p2 and p3 is convex, false otherwise.
     */
    public static boolean isConvex(Vertex p1, Vertex p2, Vertex p3) {
        double tmp = (p3.y - p1.y) * (p2.x - p1.x) - (p3.x - p1.x) * (p2.y - p1.y);
        return tmp < 0;
    }
}
