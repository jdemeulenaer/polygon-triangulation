/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package naive;
/**
 * Represents an edge between two vertices in a polygon.
 */
public class Edge {

    private Vertex source;
    private Vertex dest;

    /**
     * Create an edge.
     * @param source the Vertex the edge starts at
     * @param dest the Vertex it arrives at
     */
    public Edge(Vertex source, Vertex dest) {
        this.source = source;
        this.dest = dest;
    }

    /**
     * Return the source vertex of this edge.
     */
    public Vertex getSource() {
        return this.source;
    }
    /**
     * Return the destination vertex of this edge.
     */
    public Vertex getDestination() {
        return this.dest;
    }
}
