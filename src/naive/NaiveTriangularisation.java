/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package naive;

import java.util.List;
import java.util.ArrayList;

public class NaiveTriangularisation {
	
	private static ArrayList<Vertex> convex;
	private static ArrayList<Vertex> concave;
	public static ArrayList<Vertex> ears;
	/**
     * add diagonals to a simple polygon in order to triangulate it. 
     * Returns the list of added diagonals.
     * @param polygon the polygon to decompose
     */
	public static List<Edge> triangulateEarClipping(SimplePolygon polygon){
		Vertex[] vertices = polygon.getVertices();
		int size = vertices.length;
		/* init list of diagonals */
		List<Edge> diagonals = new ArrayList<Edge>();
		if(size <= 3) {
			return diagonals;
		}
		/* create a list of convex vertices and a list of concave(reflex) vertices */
		convex = new ArrayList<Vertex>();
		concave = new ArrayList<Vertex>();
		

		/* for each vertex determine if he's convex or concave */
		for(int i = 0; i < vertices.length - 1; i++){
			Vertex prev = vertices[i].getInEdge().getSource();
        	Vertex next = vertices[i].getOutEdge().getDestination();
        	
        	if(Vertex.isConvex(prev, next, vertices[i])){
        		convex.add(vertices[i]);
        	} 
        	else {
        		concave.add(vertices[i]);
        	}
		}
		/* determine for each convex vertex if it's an ear */
		ears = new ArrayList<Vertex>();
		for(Vertex conv : convex) {
        	if(isEar(conv)){
        		ears.add(conv);
        	}
		}
		/* Since a simple polygon always as an ear,
		 * we add diagonals between their neighbors */
		for(int i = size ;i > 3; i --){
			Vertex ear = ears.get(0);
			Vertex prev = ear.getInEdge().getSource();
        	Vertex next = ear.getOutEdge().getDestination();

        	ears.remove(ear);
        	convex.remove(ear);
        	/* we add a diagonal */
        	Edge edge = new Edge(prev,next);
        	diagonals.add(edge);
        	/* we update the update their neighbors 
        	 * and the convex,concave and ear lists */
        	prev.setOutEdge(edge);
        	next.setInEdge(edge);
        	update(prev);
        	update(next);
		}
		
		return diagonals;
	}

	private static double sign (Vertex p1, Vertex p2, Vertex p3)
	{
    	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
	}
	/**
	 * @param pt the position we check whether it's inside or outside the triangle
	 * @param v1 a point of the triangle
	 * @param v2 a point of the triangle
	 * @param v3 a point of the triangle
	 * @return true if pt is in the triangle, false otherwise.
	 */
	public static boolean pointInTriangle (Vertex pt, Vertex v1, Vertex v2, Vertex v3)
	{
	    boolean b1, b2, b3;

	    b1 = sign(pt, v1, v2) < 0.0;
	    b2 = sign(pt, v2, v3) < 0.0;
	    b3 = sign(pt, v3, v1) < 0.0;

	    return ((b1 == b2) && (b2 == b3));
	}

	private static boolean isEar(Vertex conv) {
		/* this function returns if the convex vertex conv is an ear */
		Vertex prev = conv.getInEdge().getSource();
    	Vertex next = conv.getOutEdge().getDestination();
    	int f = 0;

    	for(Vertex conc : concave){
    		if(pointInTriangle(conc,prev,next,conv)){
    			f++;
    			break;
    		}
    	}
    	return (f == 0);
	}

	private static void update(Vertex v){
		/* if the vertex was reflex we check if it has become convex and ear */
		if(concave.indexOf(v) != -1){
			Vertex prev = v.getInEdge().getSource();
    		Vertex next = v.getOutEdge().getDestination();
    		if(Vertex.isConvex(prev, next, v)){
        		convex.add(v);
        		concave.remove(v);
        		if(isEar(v)){
        			ears.add(v);
        		}
        	}
		}
		/* if vertex was convex check if he becomes an ear */
		if(convex.indexOf(v) != -1 && isEar(v)){
			ears.add(v);
		}
	}
}