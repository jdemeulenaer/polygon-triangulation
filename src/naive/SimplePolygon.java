/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package naive;

import geometry.Vector2D;
/**
 * Represents a simple polygon composed of vertices linked by edges.
 */
public class SimplePolygon {

    private Vertex[] vertices;
    private Edge[] edges;
    /**
     * Create a simple polygon.
     * @param vertices the coordinates of the vertices of the polygon.
     */
    public SimplePolygon(Vector2D[] vertices) {
        /* Initialize arrays */
        int size = vertices.length;
        this.vertices = new Vertex[size];
        this.edges = new Edge[size];

        /* Adding vertices */
        for (int i = 0; i < size; i++) {
            this.vertices[i] = new Vertex(vertices[i].x, vertices[i].y);
        }

        /* Adding edges */
        for (int i = 0; i < size; i++) {
            int j = (i + 1) % size;
            Edge newEdge = new Edge(this.vertices[i], this.vertices[j]);

            /* Set in and out edges for each vertex */
            this.vertices[i].setOutEdge(newEdge);
            this.vertices[j].setInEdge(newEdge);

            /* Store the edges in the arrays */
            this.edges[i] = newEdge;
        }
    }
    /**
     * Returns the vertices of the polygon.
     */
    public Vertex[] getVertices() {
        return this.vertices;
    }
    
    /**
     * Returns the edges of the polygon.
     */
    public Edge[] getEdges() {
        return this.edges;
    }
}

