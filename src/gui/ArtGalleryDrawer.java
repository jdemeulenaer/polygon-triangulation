/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gui;

import geometry.Vector2D;
import benchmark.PolygonGenerator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class ArtGalleryDrawer extends JFrame implements MouseListener {

    public static final int WIDTH = 500;
    public static final int HEIGHT = 570;

    private List<Vector2D> points;

    private boolean isFirst = true;
    private boolean isClosed = false;

    private int firstX;
    private int firstY;
    private int previousX;
    private int previousY;

    public ArtGalleryDrawer() {
        super("Draw your polygon in CCW order");
        points = new ArrayList<Vector2D>();

        setSize(WIDTH, HEIGHT);
        JPanel topBox = new JPanel();
        JPanel bottomBox = new JPanel();
        bottomBox.setLayout(new BoxLayout(bottomBox, BoxLayout.LINE_AXIS));
        JButton buttonGenerate = new JButton("Generate");
        bottomBox.add(buttonGenerate);

        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        this.getContentPane().add(topBox);
        this.getContentPane().add(bottomBox);

        /* ----- Listeners ----- */
        addMouseListener(this);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });
        buttonGenerate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int nbNodes = Integer.parseInt(JOptionPane.showInputDialog("Please enter the number of nodes in the polygon :"));
                generatePolygon(nbNodes);
            }
        });

        setVisible(true);
    }

    public static void main(String[] args) {
        new ArtGalleryDrawer();
    }

    private void generatePolygon(int nbNodes) {
    	Vector2D[] points = PolygonGenerator.generateVertices(0, 0, 1000, 1, 1, nbNodes);
        List<Vector2D> pointsList = Arrays.asList(points);
        Collections.reverse(pointsList);
        new ArtGalleryGUI(pointsList);
    }

    private void handleLeftClick(int x, int y) {
        if (isClosed) {
            return;
        }

        if (isFirst) {
            firstX = x;
            firstY = y;
            isFirst = false;
        }
        else {
            Graphics g = getGraphics();
            g.drawLine(previousX, previousY, x, y);
        }
        previousX = x;
        previousY = y;
        points.add(new Vector2D(x, y));
    }

    private void handleRightClick() {
        if (isFirst) {
            return;
        }

        if (!isClosed) {
            Graphics g = getGraphics();
            g.drawLine(previousX, previousY, firstX, firstY);
            isClosed = true;
            openArtGallery();
        }
        else {
            clear();
        }
    }

    public void openArtGallery() {
        new ArtGalleryGUI(points);
    }

    public void clear() {
        Graphics g = getGraphics();
        g.clearRect(0, 0, WIDTH, HEIGHT);
        isFirst = true;
        isClosed = false;
        points.clear();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            handleLeftClick(e.getX(), e.getY());
        }
        else if (SwingUtilities.isRightMouseButton(e)) {
            handleRightClick();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
