/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gui;

import geometry.*;
import triangularisation.MonotoneDecomposition;
import triangularisation.TriangulateMono;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ArtGalleryPanel extends JPanel {

    private static final int BORDER = 10;
    private static final int HEIGHT_PADDING = 23;

    private boolean isMonotone = false;
    private boolean isTriangulated = false;
    private boolean isColored = false;
    private boolean showCameras = false;

    private DoublyConnectedEdgeList highlight;
    private DoublyConnectedEdgeList startingPolygon;
    private List<DoublyConnectedEdgeList> polygonsToDraw;
    private Map<String, Integer> verticesColors;
    private int[] nbColors = new int[3];
    private Color[] colors = new Color[] { Color.red, Color.green, Color.blue };
    private int bestColor = 0;

    /* Bounds for drawing */
    private int width, height;
    private double minX, minY, deltaX, deltaY;
    
    public ArtGalleryPanel(List<Vector2D> points) {
        Vector2D[] pointsArray = new Vector2D[points.size()];
        double startingMaxY = 0.0;
        for (Vector2D v : points) {
            startingMaxY = Math.max(startingMaxY, v.y);
        }

        int index = 0;
        for (Vector2D v : points) {
            Vector2D newPoint = new Vector2D(v.x, startingMaxY - v.y);
            pointsArray[index++] = newPoint;
        }

        this.startingPolygon =  new DoublyConnectedEdgeList(pointsArray);
        this.polygonsToDraw = new ArrayList<DoublyConnectedEdgeList>();
        this.highlight = null;
        polygonsToDraw.add(startingPolygon);
    }

    public void makeMonotone() {
        if (isMonotone) {
            return;
        }

        MonotoneDecomposition.makeMonotone(startingPolygon);
        polygonsToDraw = startingPolygon.decompose();
        this.repaint();
        isMonotone = true;
    }

    public void triangulate() {
        if (isTriangulated) {
            return;
        }

        if (!isMonotone) {
            makeMonotone();
        }

        List<DoublyConnectedEdgeList> triangles = new ArrayList<DoublyConnectedEdgeList>();
        for (DoublyConnectedEdgeList monotonePolygon : polygonsToDraw) {
            TriangulateMono.triangulateMonotonePolygon(monotonePolygon);
            for (DoublyConnectedEdgeList triangle : monotonePolygon.decompose()) {
                triangles.add(triangle);
            }
        }

        polygonsToDraw = triangles;
        this.repaint();
        isTriangulated = true;
    }

    private void assignColors(Triangle triangle) {
        Vertex[] verticesArray = triangle.getVertices();
        Integer color0 = verticesColors.get(verticesArray[0].hashString());
        Integer color1 = verticesColors.get(verticesArray[1].hashString());
        Integer color2 = verticesColors.get(verticesArray[2].hashString());

        if (color0 != null) {
            if (color1 != null) {
                int newColor = 3 - color0 - color1;
                verticesColors.put(verticesArray[2].hashString(), newColor);
                nbColors[newColor] += 1;
            }
            else if (color2 != null) {
                int newColor = 3 - color0 - color2;
                verticesColors.put(verticesArray[1].hashString(), newColor);
                nbColors[newColor] += 1;
            }
            else {
                int newColor1 = 3 - color0;
                int newColor2 = 3 - color0 - newColor1;
                verticesColors.put(verticesArray[1].hashString(), newColor1);
                verticesColors.put(verticesArray[2].hashString(), newColor2);
                nbColors[newColor1] += 1;
                nbColors[newColor2] += 1;
            }
        }
        else {
            if (color1 != null && color2 != null) {
                int newColor = 3 - color1 - color2;
                verticesColors.put(verticesArray[0].hashString(), newColor);
                nbColors[newColor] += 1;
            }
            else if (color1 != null) {
                int newColor0 = 3 - color1;
                int newColor2 = 3 - color1 - newColor0;
                verticesColors.put(verticesArray[0].hashString(), newColor0);
                verticesColors.put(verticesArray[2].hashString(), newColor2);
                nbColors[newColor0] += 1;
                nbColors[newColor2] += 1;
            }
            else if (color2 != null) {
                int newColor0 = 3 - color2;
                int newColor1 = 3 - color2 - newColor0;
                verticesColors.put(verticesArray[0].hashString(), newColor0);
                verticesColors.put(verticesArray[1].hashString(), newColor1);
                nbColors[newColor0] += 1;
                nbColors[newColor1] += 1;
            }
            else {
                verticesColors.put(verticesArray[0].hashString(), 0);
                verticesColors.put(verticesArray[1].hashString(), 1);
                verticesColors.put(verticesArray[2].hashString(), 2);
                nbColors[0] += 1;
                nbColors[1] += 1;
                nbColors[2] += 1;
            }
        }
    }

    private void colorBFS(Triangle triangle) {
        triangle.visited = true;
        assignColors(triangle);
        for (Triangle neighbour : triangle.getNeighbours()) {
            if (!neighbour.visited) {
                colorBFS(neighbour);
            }
        }
    }

    public void coloring() {
        if (isColored) {
            return;
        }

        if (!isTriangulated) {
            triangulate();
        }

        /* Convert DCEL into triangles */
        List<Triangle> triangles = new ArrayList<Triangle>(polygonsToDraw.size());
        for (DoublyConnectedEdgeList triangleDCEL : polygonsToDraw) {
            assert(triangleDCEL.getVertices().size() == 3);
            Triangle triangle = new Triangle(triangleDCEL.getVertices());
            triangles.add(triangle);
        }

        /* Mapping edges to their adjacent triangles */
        Map<String, List<Triangle>> edgesToTriangle = new HashMap<String, List<Triangle>>();
        for (Triangle triangle : triangles) {
            String[] edgesHashStrings = triangle.getEdgesHashStrings();
            for (String hashString : edgesHashStrings) {
                if (!edgesToTriangle.containsKey(hashString)) {
                    edgesToTriangle.put(hashString, new ArrayList<Triangle>(2));
                }
                edgesToTriangle.get(hashString).add(triangle);
            }
        }

        /* Adding neighbours to the triangles */
        for (List<Triangle> adjacentTriangles : edgesToTriangle.values()) {
            if (adjacentTriangles.size() == 2) {
                Triangle t1 = adjacentTriangles.get(0);
                Triangle t2 = adjacentTriangles.get(1);
                t1.addNeighbour(t2);
                t2.addNeighbour(t1);
            }
        }

        /* Breadth first search on first triangle */
        Triangle root = triangles.get(0);
        verticesColors = new HashMap<String, Integer>();
        colorBFS(root);

        /* Compute best color to put cameras on */
        int minNodes = Integer.MAX_VALUE;
        for (int i = 0; i < 3; i++) {
            if (nbColors[i] < minNodes) {
                bestColor = i;
                minNodes = nbColors[i];
            }
        }

        this.repaint();
        isColored = true;
    }

    public void putCameras() {
        if (!isColored) {
            coloring();
        }
        showCameras = !showCameras;
        this.repaint();
    }

    public void localize(int x, int y) {
        if(!isTriangulated) {
            return;
        }
        double mapX = getInverseX(x);
        double mapY = getInverseY(y);
        Vector2D loc = new Vector2D(mapX, mapY);
        highlight = triangleOfPoint(loc);
        this.repaint();
    }

    /**
     * Finds a point inside a triangulated polygon and returns which triangle it's in.
     * @param loc the point to localize
     * @return the DCEL representing the triangle point loc is inside, or null otherwise.
     */
    private DoublyConnectedEdgeList triangleOfPoint(Vector2D loc) {
        for(DoublyConnectedEdgeList polygon: polygonsToDraw) {
            for (Face face : polygon.getFaces()) {
                HalfEdge left = face.edge;
                Vector2D[] vertices = new Vector2D[3];
                vertices[0] = left.origin.coord;
                left = left.next;
                int j = 1;
                while (left != face.edge) {
                    vertices[j++] = left.origin.coord;
                    left = left.next;
                }

                if (pointInTriangle(loc, vertices[0], vertices[1], vertices[2])) {
                    return new DoublyConnectedEdgeList(vertices);
                }
            }
        }
        return null;
    }

    private double sign (Vector2D p1, Vector2D p2, Vector2D p3) {
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
    }

    private boolean pointInTriangle (Vector2D pt, Vector2D v1, Vector2D v2, Vector2D v3) {
        boolean b1, b2, b3;

        b1 = sign(pt, v1, v2) < 0.0;
        b2 = sign(pt, v2, v3) < 0.0;
        b3 = sign(pt, v3, v1) < 0.0;

        return ((b1 == b2) && (b2 == b3));
    }

    private void drawCircle(Graphics g, int x, int y, int radius) {
        g.fillOval(x - radius, y - radius, radius * 2, radius * 2);
    }

    private void updateBounds() {
        this.width = this.getWidth() - 2 * BORDER;
        this.height = this.getHeight() - 2 * BORDER - HEIGHT_PADDING;

        double maxX, maxY;
        minX = Double.MAX_VALUE;
        minY = Double.MAX_VALUE;
        maxX = Double.MIN_VALUE;
        maxY = Double.MIN_VALUE;
        for (DoublyConnectedEdgeList polygon : this.polygonsToDraw) {
            List<Vertex> vertices = polygon.getVertices();
            for (Vertex vertex : vertices) {
                if (vertex.coord.x > maxX)
                    maxX = vertex.coord.x;
                if (vertex.coord.y > maxY)
                    maxY = vertex.coord.y;
                if (vertex.coord.x < minX)
                    minX = vertex.coord.x;
                if (vertex.coord.y < minY)
                    minY = vertex.coord.y;
            }
        }
        deltaX = maxX - minX;
        deltaY = maxY - minY;
    }

    private int getCorrespondingX(double x) {
        return 10 + (int) (x * width / deltaX - minX * width / deltaX);
    }

    private int getCorrespondingY(double y) {
        return height + 10 + HEIGHT_PADDING - (int) (y * height / deltaY - minY * height / deltaY);
    }

    private double getInverseX(int x) {
        return (x - 10 +  minX * width / deltaX) * deltaX / width;
    }

    private double getInverseY(int y) {
        return (height - y + 10 + HEIGHT_PADDING + minY * height / deltaY) * deltaY / height;
    }

    public void paint(Graphics g) {
        this.updateBounds();

        for (DoublyConnectedEdgeList polygon : this.polygonsToDraw) {
            List<HalfEdge> edges = polygon.getEdges();
            for (HalfEdge edge : edges) {
                Vertex startVertex = edge.getOrigin();
                Vertex endVertex = edge.getDestination();
                int startX = getCorrespondingX(startVertex.coord.x);
                int startY = getCorrespondingY(startVertex.coord.y);
                int endX = getCorrespondingX(endVertex.coord.x);
                int endY = getCorrespondingY(endVertex.coord.y);
                g.drawLine(startX, startY, endX, endY);
            }

            if (isColored) {
                List<Vertex> vertices = polygon.getVertices();
                for (Vertex vertex : vertices) {
                    int colorNb = verticesColors.get(vertex.hashString());
                    if (!showCameras || colorNb == bestColor) {
                        Color color = colors[colorNb];
                        g.setColor(color);
                        int x = getCorrespondingX(vertex.coord.x);
                        int y = getCorrespondingY(vertex.coord.y);
                        drawCircle(g, x, y, 10);
                    }
                }
                g.setColor(Color.black);
            }
        }

        if (this.highlight != null){
	        List<Vertex> hVertices = highlight.getVertices();
	        int i = 0;
	        int[] xCoord = new int[3];
	        int[] yCoord = new int[3];
	        for(Vertex v : hVertices){
	        	xCoord[i] = getCorrespondingX(v.coord.x);
	        	yCoord[i] = getCorrespondingY(v.coord.y);
	        	i++;
	        }
	        g.fillPolygon(xCoord, yCoord, 3);
        }
    }
}
