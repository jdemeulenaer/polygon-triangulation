/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gui;

import geometry.Vector2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.util.List;

public class ArtGalleryGUI extends JFrame implements MouseListener {

    public static final int WIDTH = 500;
    public static final int HEIGHT = 560;

    private ArtGalleryPanel topBox;

    public ArtGalleryGUI(List<Vector2D> points) {
        setTitle("The Art Gallery Problem");
        setSize(new Dimension(WIDTH, HEIGHT));
        setVisible(true);

        this.topBox = new ArtGalleryPanel(points);
        JPanel bottomBox = new JPanel();
        bottomBox.setLayout(new BoxLayout(bottomBox, BoxLayout.LINE_AXIS));
        JButton buttonMonotone = new JButton("Make monotone");
        JButton buttonTriangle = new JButton("Triangulate");
        JButton buttonColoring = new JButton("3-coloring");
        JButton buttonCameras = new JButton("Put cameras");
        bottomBox.add(buttonMonotone);
        bottomBox.add(buttonTriangle);
        bottomBox.add(buttonColoring);
        bottomBox.add(buttonCameras);
   
        JPanel window = new JPanel();
        window.setLayout(new BoxLayout(window, BoxLayout.PAGE_AXIS));
        window.add(topBox);
        window.add(bottomBox);
      
        this.getContentPane().add(window);

        window.addMouseListener(this);
        /* ----- Add buttons listeners ----- */
        buttonMonotone.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                topBox.makeMonotone();
            }
        });
        buttonTriangle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                topBox.triangulate();
            }
        });
        buttonColoring.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                topBox.coloring();
            }
        });
        buttonCameras.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                topBox.putCameras();
            }
        });
    }

    public void mousePressed(MouseEvent e) {
        topBox.localize(e.getX(), e.getY());
    }

    public void mouseReleased(MouseEvent e) {}

    public void mouseEntered(MouseEvent e) {}

    public void mouseExited(MouseEvent e) {}

    public void mouseClicked(MouseEvent e) {}

}
