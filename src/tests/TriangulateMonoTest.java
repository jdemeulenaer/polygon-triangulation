/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests;

import org.junit.*;
import static org.junit.Assert.*;
import geometry.DoublyConnectedEdgeList;
import geometry.Vector2D;
import benchmark.PolygonGenerator;
import triangularisation.TriangulateMono;


public class TriangulateMonoTest {
	@Test
	public void numberOfDiagonalsTest() {

	    Vector2D[] vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, 10);
	    DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
	    int initSize = polygon.getEdges().size();
	    TriangulateMono.triangulateMonotonePolygon(polygon);
		polygon.decompose();
		assertEquals("A triangulation of a polygon with 10 vertices consists of 7 diagonals (14 half edges) added", 14, polygon.getEdges().size() - initSize);
	   
	    vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, 60);
	    polygon = new DoublyConnectedEdgeList(vertices);
	    initSize = polygon.getEdges().size();
	    TriangulateMono.triangulateMonotonePolygon(polygon);
		polygon.decompose();
		assertEquals("A triangulation of a polygon with 60 vertices consists of 57 (114 halfedges) diagonals added", 114, polygon.getEdges().size() - initSize );
	   
	    vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, 3);
	    polygon = new DoublyConnectedEdgeList(vertices);
	    initSize = polygon.getEdges().size();
	    TriangulateMono.triangulateMonotonePolygon(polygon);
		polygon.decompose();
	    assertEquals("A triangulation of a polygon with 3 vertices consists of 0 diagonals added", 0, polygon.getEdges().size() - initSize );
	}
	
	
	@Test
	public void numberOfTrianglesTest() {
	    Vector2D[] vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, 10);
	    DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
	    TriangulateMono.triangulateMonotonePolygon(polygon);
		polygon.decompose();
		assertEquals("A triangulation of a polygon with 10 vertices consists of 8 triangles", 8, polygon.getFaces().size());
	   
	    vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, 100);
	    polygon = new DoublyConnectedEdgeList(vertices);
	    TriangulateMono.triangulateMonotonePolygon(polygon);
		polygon.decompose();
		assertEquals("A triangulation of a polygon with 100 vertices consists of 98 triangles", 98, polygon.getFaces().size() );
	   
	    vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, 3);
	    polygon = new DoublyConnectedEdgeList(vertices);
	    TriangulateMono.triangulateMonotonePolygon(polygon);
		polygon.decompose();
	    assertEquals("A triangulation of a polygon with 3 vertices consists of 1 triangles", 1, polygon.getFaces().size() );
	   
	}
	
	@Test
	public void numberOfVertexTest() {
		Vector2D[] vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, 10);
		DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
		TriangulateMono.triangulateMonotonePolygon(polygon);
		assertEquals("A triangulation of a polygon with 10 vertices keeps its 10 vertices", 10, polygon.getVertices().size() );
	}
	
}
