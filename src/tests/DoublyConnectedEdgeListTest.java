/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests;

import benchmark.PolygonGenerator;
import geometry.DoublyConnectedEdgeList;
import geometry.Vector2D;
import geometry.Vertex;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DoublyConnectedEdgeListTest {

    @Test
    public void doublyConnectedEdgeListInitTest() {
        for (int n = 3; n <= 15; ++n) {
            Vector2D[] vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 1, n);
            DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);

            assertEquals("number of vertices is n", polygon.getVertices().size(), n);
            assertEquals("number of half-edges is 2 * n", polygon.getEdges().size(), 2 * n);
            assertEquals("number of faces is 1", polygon.getFaces().size(), 1);
        }
    }

    @Test
    public void addDiagonalAndDecompositionTest() {
        for (int n = 4; n <= 15; ++n) {
            Vector2D[] vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 0, n);
            DoublyConnectedEdgeList convexPolygon = new DoublyConnectedEdgeList(vertices);
            List<Vertex> polygonVertices = convexPolygon.getVertices();

            int diagonalsAdded = 0;
            for (int i = 0; i < n - 2; i += 2) {
                Vertex v1 = polygonVertices.get(i);
                Vertex v2 = polygonVertices.get(i + 2);
                convexPolygon.addDiagonal(v1, v2);
                diagonalsAdded++;
            }

            assertEquals("number of sub-polygons", convexPolygon.decompose().size(), diagonalsAdded + 1);
        }
    }

}
