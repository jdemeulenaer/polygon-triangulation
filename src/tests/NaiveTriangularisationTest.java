/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests;

import java.util.List;

import naive.Edge;
import naive.NaiveTriangularisation;
import naive.SimplePolygon;
import naive.Vertex;

import org.junit.*;
import benchmark.PolygonGenerator;
import geometry.Vector2D;
import static org.junit.Assert.*;

public class NaiveTriangularisationTest {

	@Test
	public void numberOfDiagonals() {
		Vector2D[] vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 1, 10);
	    SimplePolygon polygon = new SimplePolygon(vertices);
	    List<Edge> diagonals = NaiveTriangularisation.triangulateEarClipping(polygon);
	    assertEquals("A triangulation of a polygon with 10 vertices consists of 7 diagonals added",7,diagonals.size());
	    
	    vertices = PolygonGenerator.generateVertices(0, 0, 100, 1, 1, 3);
	    polygon = new SimplePolygon(vertices);
	    diagonals = NaiveTriangularisation.triangulateEarClipping(polygon);
	    assertEquals("A triangulation of a polygon with 3 vertices consists of 0 diagonals added",0,diagonals.size());
	}
	
	@Test
	public void isPointInsideTriangle() {
		Vertex t1 = new Vertex(0.0,2.0);
		Vertex t2 = new Vertex(2.0,0.0);
		Vertex t3 = new Vertex(0.0,0.0);
		
		assertEquals("Point is inside the triangle",true, NaiveTriangularisation.pointInTriangle(new Vertex(0.5,0.5),t1,t2,t3));		
		assertEquals("Point is outside the triangle",false, NaiveTriangularisation.pointInTriangle(new Vertex(1.0,1.0),t1,t2,t3));		
	} 
}
