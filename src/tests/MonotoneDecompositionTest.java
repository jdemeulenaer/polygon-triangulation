/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests;

import geometry.DoublyConnectedEdgeList;
import geometry.Vector2D;
import triangularisation.MonotoneDecomposition;

import org.junit.*;
import static org.junit.Assert.*;


public class MonotoneDecompositionTest {

    @Test
    public void makeMonotoneTestSplitVertex() {
        Vector2D[] vertices = new Vector2D[] {
                new Vector2D(0.0, 0.0),
                new Vector2D(1.0, 1.0),
                new Vector2D(2.0, 0.0),
                new Vector2D(1.0, 2.0)
        };
        DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
        MonotoneDecomposition.makeMonotone(polygon);

        assertEquals("should add diagonal adjacent to split vertex", 2, polygon.decompose().size());
    }

    @Test
    public void makeMonotoneTestMergeAndSplitVertex() {
        Vector2D[] vertices = new Vector2D[] {
                new Vector2D(0.0, 0.0),
                new Vector2D(1.0, 1.0),
                new Vector2D(2.0, 0.0),
                new Vector2D(2.0, 3.0),
                new Vector2D(1.0, 2.0),
                new Vector2D(0.0, 3.0)
        };
        DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
        MonotoneDecomposition.makeMonotone(polygon);

        assertEquals("should add diagonal adjacent to merge and split vertices", 2, polygon.decompose().size());
    }

    @Test
    public void makeMonotoneTestMergeAndEndVertex() {
        Vector2D[] vertices = new Vector2D[] {
                new Vector2D(0.0, 2.0),
                new Vector2D(1.0, 0.0),
                new Vector2D(2.0, 2.0),
                new Vector2D(1.0, 1.0)
        };
        DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
        MonotoneDecomposition.makeMonotone(polygon);

        assertEquals("should add diagonal adjacent to merge and end vertices", 2, polygon.decompose().size());
    }

    @Test
    public void makeMonotoneTestMergeAndRegularLeftVertex() {
        Vector2D[] vertices = new Vector2D[] {
                new Vector2D(0.0, 1.0),
                new Vector2D(2.0, 0.0),
                new Vector2D(2.0, 3.0),
                new Vector2D(1.0, 2.0),
                new Vector2D(0.0, 3.0)
        };
        DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
        MonotoneDecomposition.makeMonotone(polygon);

        assertEquals("should add diagonal adjacent to merge and regular left vertices", 2, polygon.decompose().size());
    }

    @Test
    public void makeMonotoneTestMergeAndRegularRightVertex() {
        Vector2D[] vertices = new Vector2D[] {
                new Vector2D(0.0, 0.0),
                new Vector2D(2.0, 1.0),
                new Vector2D(2.0, 3.0),
                new Vector2D(1.0, 2.0),
                new Vector2D(0.0, 3.0),
        };
        DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
        MonotoneDecomposition.makeMonotone(polygon);

        assertEquals("should add diagonal adjacent to merge and regular right vertices", 2, polygon.decompose().size());
    }

    @Test
    public void makeMonotoneTestBookExample() {
        Vector2D[] vertices = {
                new Vector2D(13.0, 8.0),
                new Vector2D(11.0, 6.0),
                new Vector2D(10.0, 12.0),
                new Vector2D(8.0, 11.0),
                new Vector2D(6.0, 12.0),
                new Vector2D(2.0, 10.0),
                new Vector2D(5.0, 9.0),
                new Vector2D(4.0, 5.0),
                new Vector2D(1.0, 7.0),
                new Vector2D(0.0, 3.0),
                new Vector2D(3.0, 1.0),
                new Vector2D(7.0, 2.0),
                new Vector2D(10.0, 0.0),
                new Vector2D(9.0, 4.0),
                new Vector2D(12.0, 3.0)
        };
        DoublyConnectedEdgeList polygon = new DoublyConnectedEdgeList(vertices);
        MonotoneDecomposition.makeMonotone(polygon);

        assertEquals("should decompose into 5 faces", 5, polygon.decompose().size());
    }

}
