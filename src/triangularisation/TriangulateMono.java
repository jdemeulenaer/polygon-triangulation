/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package triangularisation;

import geometry.Vertex;
import geometry.Vertex.Chain;
import geometry.DoublyConnectedEdgeList;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class TriangulateMono {

	/**
     * Add diagonals to a y-monotone polygon such that it is decomposed into triangles.
     * @param polygon the y-monotone polygon to decompose
     */
	public static void triangulateMonotonePolygon(DoublyConnectedEdgeList polygon){
		/* Create a sequence of the vertices sorted by y-coordinates*/
		List<Vertex> vertices = polygon.getVertices();
		if(vertices.size() > 3) {
			Collections.sort(vertices);
			
			/* Assigning chain to vertices */
			Vertex curr = vertices.get(0);
			while(curr != vertices.get(vertices.size() - 1)){
				curr = curr.getOutEdge().getDestination();
				curr.chain = Chain.LEFT;
			}

			curr = vertices.get(0);
			while(curr != vertices.get(vertices.size() -1)){
				curr = curr.getInEdge().getOrigin();
				curr.chain = Chain.RIGHT;
			}
			
			/* Initialize the stack */
			Stack<Vertex> vStack = new Stack<Vertex>();
			vStack.push(vertices.get(0));
			vStack.push(vertices.get(1));
			
			for(int i = 2; i < vertices.size() - 1; i++){
				Vertex currentVertex = vertices.get(i);
				if(vertices.get(i).chain != vStack.peek().chain){
					/* Next vertex is on a different chain than vertices on the stack */
					handleDifferentChainVertex(vStack, currentVertex, vertices.get(i - 1), polygon);
				}
				else {
					/* Next vertex is on the same chain than vertices on the stack */
					handleSameChainVertex(vStack, currentVertex, polygon);
				}
			}
			/* handling last vertex */
			Vertex last = vertices.get(vertices.size() - 1);
			vStack.pop();
			while(vStack.size() > 1){
				Vertex top = vStack.pop();
				polygon.addDiagonal(last, top);
			}
		}
	}

	private static void handleDifferentChainVertex(Stack<Vertex> st, Vertex v1, Vertex v2, DoublyConnectedEdgeList polygon){
		Vertex top = st.pop();
		/* Pop all vertices from the stack and insert diagonal to each popped vertex except the last one*/
		while(!st.empty()){
        	polygon.addDiagonal(v1,top);
            top = st.pop();
		}

		/* Push the to lowest vertices who might 
		 * still need diagonals on the stack */
		st.push(v2);
		st.push(v1);
	}

	private static void handleSameChainVertex(Stack<Vertex> st, Vertex v, DoublyConnectedEdgeList polygon) {
		/* Pop one vertex from the stack (the neighbor of v)*/
		Vertex lastPopped = st.pop();
		/* Pop vertices while a diagonal from v to them is inside the polygon */
		while (!st.isEmpty()) {
			Vertex v2 = st.pop();
			if (v.chain == Chain.RIGHT && Vertex.isConvex(lastPopped, v, v2)) {
				polygon.addDiagonal(v, v2);
			} else if (v.chain == Chain.LEFT && Vertex.isConvex(lastPopped, v2, v)) {
				polygon.addDiagonal(v, v2);
			}
			else {
				st.push(v2);
				break;
			}
			lastPopped = v2;
		}

		/* Push back v and the last vertex for which a diagonal was added 
		 * (or the neighbor of v if none was added) on the stack */
		st.push(lastPopped);
		st.push(v);
	}
}
