/*
 * Copyright (C) 2015  Demeulenaere Jordan & Dieuzeide Thomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package triangularisation;

import geometry.*;
import java.util.List;
import java.util.PriorityQueue;
import java.util.TreeMap;

public class MonotoneDecomposition {

    /**
     * Add diagonals to a simple polygon such that it is decomposed into y-monotone subpolygons.
     * @param polygon the polygon to decompose
     */
    public static void makeMonotone(DoublyConnectedEdgeList polygon) {
        List<Vertex> vertices = polygon.getVertices();

        /* Init the priority queue and search tree */
        PriorityQueue<Vertex> queue = new PriorityQueue<Vertex>(vertices);
        TreeMap<HalfEdge, Vertex> helpers = new TreeMap<HalfEdge, Vertex>();

        /* Handling each vertex one by one */
        Vertex currentVertex;
        while(!queue.isEmpty()) {
            currentVertex = queue.remove();
            switch (currentVertex.getType()) {
                case REGULAR_LEFT:
                    handleRegularLeftVertex(currentVertex, helpers, polygon);
                    break;
                case REGULAR_RIGHT:
                    handleRegularRightVertex(currentVertex, helpers, polygon);
                    break;
                case START:
                    handleStartVertex(currentVertex, helpers);
                    break;
                case END:
                    handleEndVertex(currentVertex, helpers, polygon);
                    break;
                case MERGE:
                    handleMergeVertex(currentVertex, helpers, polygon);
                    break;
                case SPLIT:
                    handleSplitVertex(currentVertex, helpers, polygon);
                    break;
            }
        }
    }

    private static void insertIfPreviousMerge(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers, DoublyConnectedEdgeList polygon) {
        HalfEdge inEdge = vertex.getInEdge();
        Vertex helperInEdge = helpers.get(inEdge);
        if (helperInEdge.getType() == Vertex.VertexType.MERGE) {
            polygon.addDiagonal(vertex, helperInEdge);
        }
        helpers.remove(inEdge);
    }

    private static HalfEdge findEdgeOnTheLeft(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers) {
        HalfEdge newEdge = new HalfEdge();
        newEdge.origin = vertex;
        newEdge.next = newEdge;
        return helpers.lowerKey(newEdge);
    }

    private static void handleStartVertex(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers) {
        helpers.put(vertex.getOutEdge(), vertex);
    }

    private static void handleEndVertex(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers, DoublyConnectedEdgeList polygon) {
        insertIfPreviousMerge(vertex, helpers, polygon);
    }

    private static void handleSplitVertex(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers, DoublyConnectedEdgeList polygon) {
        HalfEdge leftEdge = findEdgeOnTheLeft(vertex, helpers);
        Vertex helperLeftEdge = helpers.get(leftEdge);
        helpers.put(leftEdge, vertex);
        polygon.addDiagonal(vertex, helperLeftEdge);
        helpers.put(vertex.getOutEdge(), vertex);
    }

    private static void handleMergeVertex(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers, DoublyConnectedEdgeList polygon) {
        insertIfPreviousMerge(vertex, helpers, polygon);
        HalfEdge leftEdge = findEdgeOnTheLeft(vertex, helpers);
        Vertex helperLeftEdge = helpers.get(leftEdge);
        if (helperLeftEdge.getType() == Vertex.VertexType.MERGE) {
            polygon.addDiagonal(vertex, helperLeftEdge);
        }
        helpers.put(leftEdge, vertex);
    }

    private static void handleRegularLeftVertex(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers, DoublyConnectedEdgeList polygon) {
        HalfEdge leftEdge = findEdgeOnTheLeft(vertex, helpers);
        Vertex helperLeftEdge = helpers.get(leftEdge);
        if (helperLeftEdge.getType() == Vertex.VertexType.MERGE) {
            polygon.addDiagonal(vertex, helperLeftEdge);
        }
        helpers.put(leftEdge, vertex);
    }

    private static void handleRegularRightVertex(Vertex vertex, TreeMap<HalfEdge, Vertex> helpers, DoublyConnectedEdgeList polygon) {
        insertIfPreviousMerge(vertex, helpers, polygon);
        helpers.put(vertex.getOutEdge(), vertex);
    }

}
